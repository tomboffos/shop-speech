<?php

namespace App\Controllers;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;
use Google_Client;

class StaticPagesController{
	
	public static function serveLogin(){
		 view('login');
	}

    public static function serveHome(){
        view('home',['somedata'=>["this","is","awesome"]]);
    }

}