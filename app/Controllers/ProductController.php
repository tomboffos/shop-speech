<?php


namespace App\Controllers;


use FFMpeg\FFMpeg;
use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;

class ProductController
{
    public function createVoice()
    {
        $description = $_GET['description'];
        $chars = $_GET['characteristics'];
        $price = $_GET['price'];
        $review_count = $_GET['review_count'];
        $audioText = '';
        $textToSpeechClient = new TextToSpeechClient();
        $input = new SynthesisInput();
        if ($description != '')
            $audioText = $audioText . "Описание товара : $description";

        if ($chars != '')
            $audioText = $audioText . " Характеристика товара: $chars";

        if ($price != '')
            $audioText = $audioText . " Цена товара: $price";

        if ($review_count != '')
            $audioText = $audioText . " Количество отзывов: $review_count";
        $input->setText($audioText);
        $voice = new VoiceSelectionParams();
        $voice->setLanguageCode('ru-RU');
        $audioConfig = new AudioConfig();
        $audioConfig->setAudioEncoding(AudioEncoding::MP3);

        $resp = $textToSpeechClient->synthesizeSpeech($input, $voice, $audioConfig);
        $rand = mt_rand(0, 99999);
        file_put_contents("public/test$rand.mp3", $resp->getAudioContent());

        return view('product', [
            'product' => [
                'description' => $description,
                'chars' => $chars,
                'price' => $price,
                'review_count' => $review_count,
                'audio_path' => $audioText != '' ? "/public/test$rand.mp3" : '/public/default.mp3'
            ]
        ]);
    }

    public function generateVideo()
    {
        $rand = mt_rand(0, 10000000);
        mkdir("public/products$rand", 0755, true);

        foreach ($this->reArrayFiles($_FILES['images']) as $index => $image) {
            $delArray = explode('.', $image['name']);

            $extName = $delArray[count($delArray) - 1];
            $name = str_pad($index, 3, 0, STR_PAD_LEFT);
            move_uploaded_file($image['tmp_name'], "public/products$rand/{$name}.{$extName}");

            $imagesArray[] = "public/products$rand/{$image['name']}";
        }
        $audio = substr($_POST['audio'], 1);
        $rate = 1/count($imagesArray);
        exec("ffmpeg -framerate $rate -i public/products$rand/%03d.png -i $audio  public/video$rand.mp4");

        header("Location: http://localhost:8000/public/video$rand.mp4");
        exit();

    }

    function reArrayFiles(&$file_post)
    {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

}