<?php
/**
 * All rights reserved.
 * User: Dread Pirate Roberts
 * Date: 28-Aug-17
 * Time: 20:24
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
putenv('GOOGLE_APPLICATION_CREDENTIALS=voice_service.json'); //your path to file of cred

require 'public/index.php';