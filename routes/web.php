<?php


Router::get('','StaticPagesController@serveLogin');
Router::get('login','StaticPagesController@serveLogin');
Router::get('home','StaticPagesController@serveHome');
Router::get('create-voice','ProductController@createVoice');
Router::post('create-video','ProductController@generateVideo');

?>